package br.sgq.norma;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import br.sgq.consumidor.ConsumidorServicos;
import br.sgq.consumidor.Endpoints;
import br.sgq.consumidor.ErroConsumoServicoException;

@Named
public class NormaService implements Serializable{
	private static final long serialVersionUID = -9041130429304417083L;
	@Inject
	private ConsumidorServicos consumidor;

	public NormaService() {
		
	}
	
	public List<Norma> consultaNormas(LocalidadeNorma localidade) {
		try {
			switch (localidade) {
				case INTERNACIONAL:
					return consumidor.consultar(Norma.class, Endpoints.CONSULTAR_NORMAS_INTERNACIONAIS);
				case NACIONAL:
					return consumidor.consultar(Norma.class, Endpoints.CONSULTAR_NORMAS_NACIONAIS);
					default: throw new IllegalArgumentException("Localidade "+localidade+" inv�lida.");
			}
		} catch (IOException exception) {
			throw new ErroConsumoServicoException(exception, "Erro ao consultar as normas");
		}
	}

}
