package br.sgq.norma;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("categoriaNormaConverter")
public class CategoriaNormaConverter implements Converter{

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return CategoriaNorma.valueOf(value);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return value.toString();
	}

}
