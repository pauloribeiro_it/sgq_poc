package br.sgq.norma;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("normaMB")
@ViewScoped
public class NormaController implements Serializable{
	
	private static final long serialVersionUID = -7389846313482151596L;
	private LocalidadeNorma localidadeNorma;
	private List<Norma> normas;
	
	@Inject
	private NormaService normaService;
	
	public LocalidadeNorma getLocalidadeNorma() {
		return localidadeNorma;
	}

	public void setLocalidadeNorma(LocalidadeNorma localidadeNorma) {
		this.localidadeNorma = localidadeNorma;
	}
	
	public void consultaNorma() {
		this.normas = normaService.consultaNormas(localidadeNorma);
	}

	public List<Norma> getNormas() {
		return normas;
	}

	public void setNormas(List<Norma> normas) {
		this.normas = normas;
	}
	
}
