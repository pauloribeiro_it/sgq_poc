package br.sgq.especialidade;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import br.sgq.consumidor.ConsumidorServicos;
import br.sgq.consumidor.Endpoints;
import br.sgq.consumidor.ResourceType;

@Named
public class EspecialidadeService {

	@Inject
	private ConsumidorServicos consumidorServicos;
	
	public List<Especialidade> consultarAssessorias() throws Exception{
		return consumidorServicos.consultar(Especialidade.class, Endpoints.CONSULTAR_ASSESSORIAS);
	}
	
	public List<Especialidade> consultarConsultorias() throws Exception{
		return consumidorServicos.consultar(Especialidade.class, Endpoints.CONSULTAR_CONSULTORIAS);
	}
	
	public String contratarConsultoria(String cnpj) throws Exception {
		return consumidorServicos.submeter(ResourceType.ESPECIALIDADE.getTipo()+"/consultoria/contratar", cnpj);
	}
	
	public String contratarAssessoria(String cnpj) throws Exception {
		return consumidorServicos.submeter(ResourceType.ESPECIALIDADE.getTipo()+"/assessoria/contratar", cnpj);
	}
}
