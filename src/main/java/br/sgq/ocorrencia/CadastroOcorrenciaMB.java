package br.sgq.ocorrencia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named("cadastroOcorrenciaMB")
@ViewScoped
public class CadastroOcorrenciaMB implements Serializable{

	private static final long serialVersionUID = -4732088166683054701L;
	
	private Ocorrencia ocorrencia = new Ocorrencia();
	private Ocorrencia ocorrenciaSelecionada = new Ocorrencia();
	private List<Ocorrencia> ocorrencias = new ArrayList<>();
	private List<Ocorrencia> ocorrenciasSelecionadas = new ArrayList<>();
	
	
	public CadastroOcorrenciaMB() {
		
	}

	public void excluiOcorrencia() {
		
	}
	
	public void detalhaOcorrencia() {
		
	}
	
	public void editarOcorrencia() {
		
	}
	
	public Ocorrencia getOcorrenciaSelecionada() {
		return ocorrenciaSelecionada;
	}

	public void setOcorrenciaSelecionada(Ocorrencia ocorrenciaSelecionada) {
		this.ocorrenciaSelecionada = ocorrenciaSelecionada;
	}

	public Ocorrencia getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(Ocorrencia ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public List<Ocorrencia> getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(List<Ocorrencia> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

	public List<Ocorrencia> getOcorrenciasSelecionadas() {
		return ocorrenciasSelecionadas;
	}

	public void setOcorrenciasSelecionadas(List<Ocorrencia> ocorrenciasSelecionadas) {
		this.ocorrenciasSelecionadas = ocorrenciasSelecionadas;
	}
	
}
