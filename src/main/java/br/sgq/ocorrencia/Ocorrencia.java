package br.sgq.ocorrencia;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="OCORRENCIA")
public class Ocorrencia {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer idOcorrencia;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="tipo_ocorrencia")
	private TipoOcorrencia tipoOcorrencia;
	
	@Column(name="data_ocorrencia")
	@Temporal(TemporalType.DATE)
	private Date dataOcorrencia;
	
	@Column(name="descricao")
	private String descricao;
	
	public Ocorrencia() {
		
	}

	public Integer getIdOcorrencia() {
		return idOcorrencia;
	}

	public void setIdOcorrencia(Integer idOcorrencia) {
		this.idOcorrencia = idOcorrencia;
	}

	public TipoOcorrencia getTipoOcorrencia() {
		return tipoOcorrencia;
	}

	public void setTipoOcorrencia(TipoOcorrencia tipoOcorrencia) {
		this.tipoOcorrencia = tipoOcorrencia;
	}

	public Date getDataOcorrencia() {
		return dataOcorrencia;
	}

	public void setDataOcorrencia(Date dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idOcorrencia == null) ? 0 : idOcorrencia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ocorrencia other = (Ocorrencia) obj;
		if (idOcorrencia == null) {
			if (other.idOcorrencia != null)
				return false;
		} else if (!idOcorrencia.equals(other.idOcorrencia))
			return false;
		return true;
	}
	
}
