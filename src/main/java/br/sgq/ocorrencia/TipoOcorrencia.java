package br.sgq.ocorrencia;

public enum TipoOcorrencia {
	INCIDENTE("Incidente"),PROBLEMA("Problema");
	
	private String descricao;
	
	private TipoOcorrencia(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
