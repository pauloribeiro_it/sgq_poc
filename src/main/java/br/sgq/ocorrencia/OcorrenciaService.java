package br.sgq.ocorrencia;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

@Named
public class OcorrenciaService implements Serializable{
	private static final long serialVersionUID = 4812729434538297740L;
	
	@Inject
	private OcorrenciaDAO dao;
	
	@Transactional(value = TxType.REQUIRES_NEW)
	public void salvar(Ocorrencia ocorrencia) {
		dao.cadastrar(ocorrencia);
	}
	
	@Transactional(value = TxType.REQUIRES_NEW)
	public void excluir(Ocorrencia ocorrencia) {
		dao.excluir(ocorrencia);
	}
	
	public Ocorrencia recuperaPorId(Integer idOcorrencia) {
		return dao.buscaPorId(idOcorrencia);
	}

	public List<Ocorrencia> recuperaOcorrencias(){
		return dao.getAll();
	}
	
	
}
