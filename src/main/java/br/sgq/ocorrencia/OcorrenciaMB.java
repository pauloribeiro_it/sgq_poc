package br.sgq.ocorrencia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("ocorrenciaMB")
@ViewScoped
public class OcorrenciaMB implements Serializable{
	private static final long serialVersionUID = 5045069592774668929L;
	
	@Inject
	private OcorrenciaService service;
	private List<Ocorrencia> ocorrencias = new ArrayList<>();
	private List<Ocorrencia> ocorrenciasSelecionadas = new ArrayList<>();
	private Ocorrencia ocorrenciaSelecionada = new Ocorrencia();
	private Ocorrencia ocorrencia = new Ocorrencia();
	
	public OcorrenciaMB () {
		
	}
	
	@PostConstruct
	public void init() {
		this.ocorrencias = service.recuperaOcorrencias();
		String idOcorrencia = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idOcorrencia");
		if(idOcorrencia != null) {
			this.ocorrencia = service.recuperaPorId(Integer.valueOf(idOcorrencia));
		}
	}
	
	public void cadastrarOcorrencia() {
		FacesContext faces = FacesContext.getCurrentInstance();
		service.salvar(ocorrencia);
		faces.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, 
				"Cadastro",	"Cadastro de ocorr�ncia conclu�do com sucesso."));
	}
	
	public void editarOcorrencia() {
		NavigationHandler nh = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
		nh.handleNavigation(FacesContext.getCurrentInstance(), null,
				"/privado/cadastroOcorrencia.xhtml?faces-redirect=true&idOcorrencia="
						+ ocorrenciaSelecionada.getIdOcorrencia());
	}
	
	public void excluirOcorrencia() {
		FacesContext faces = FacesContext.getCurrentInstance();
		NavigationHandler nh = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
		
		service.excluir(ocorrenciaSelecionada);
		
		faces.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, 
				"Cadastro",	"Exclus�o de ocorr�ncia conclu�do com sucesso."));
		nh.handleNavigation(FacesContext.getCurrentInstance(), null,
				"/privado/cadastroOcorrencia.xhtml?faces-redirect=true&idOcorrencia="
						+ ocorrenciaSelecionada.getIdOcorrencia());
	}

	public List<Ocorrencia> getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(List<Ocorrencia> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

	public Ocorrencia getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(Ocorrencia ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public Ocorrencia getOcorrenciaSelecionada() {
		return ocorrenciaSelecionada;
	}

	public void setOcorrenciaSelecionada(Ocorrencia ocorrenciaSelecionada) {
		this.ocorrenciaSelecionada = ocorrenciaSelecionada;
	}

	public List<Ocorrencia> getOcorrenciasSelecionadas() {
		return ocorrenciasSelecionadas;
	}

	public void setOcorrenciasSelecionadas(List<Ocorrencia> ocorrenciasSelecionadas) {
		this.ocorrenciasSelecionadas = ocorrenciasSelecionadas;
	}
	
}
