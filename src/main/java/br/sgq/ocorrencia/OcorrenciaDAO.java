package br.sgq.ocorrencia;

import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Named
public class OcorrenciaDAO {
	@PersistenceContext(unitName = "sgq",type=PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;
	
	public void cadastrar(Ocorrencia ocorrencia) {
		if(ocorrencia.getIdOcorrencia() != null) {
			ocorrencia = entityManager.merge(ocorrencia);
		}
		entityManager.persist(ocorrencia);
	}
	
	public void excluir(Ocorrencia ocorrencia) {
		Ocorrencia ocorrenciaRemovida = buscaPorId(ocorrencia.getIdOcorrencia());
		entityManager.remove(ocorrenciaRemovida);
	}
	
	public Ocorrencia buscaPorId(Integer id) {
		return entityManager.find(Ocorrencia.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Ocorrencia> getAll(){
		final String selectAll = "select o from Ocorrencia o";
		return entityManager.createQuery(selectAll).getResultList();
	}
}
