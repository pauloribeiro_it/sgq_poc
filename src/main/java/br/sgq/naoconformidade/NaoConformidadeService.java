package br.sgq.naoconformidade;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

@Named
public class NaoConformidadeService implements Serializable{
	private static final long serialVersionUID = 6598091608049141275L;
	
	@Inject
	private NaoConformidadeDAO dao;

	@Transactional(value = TxType.REQUIRES_NEW)
	public void salvar(NaoConformidade naoConformidade) {
		dao.salvar(naoConformidade);
	}
	
	@Transactional(value = TxType.REQUIRES_NEW)
	public void excluir(NaoConformidade naoConformidade) {
		dao.excluir(naoConformidade);
	}
	
	public NaoConformidade recuperaPorId(Integer idNaoConformidade) {
		return dao.buscaPorId(idNaoConformidade);
	}
	
	public List<NaoConformidade> recuperaNaoConformidades(){
		return dao.getAll();
	}
}
