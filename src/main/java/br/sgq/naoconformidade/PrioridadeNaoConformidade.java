package br.sgq.naoconformidade;

public enum PrioridadeNaoConformidade {
	BAIXA("Baixa"),MEDIA("M�dia"),ALTA("Alta");
	
	PrioridadeNaoConformidade(String descricao){
		this.descricao = descricao;
	}
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
