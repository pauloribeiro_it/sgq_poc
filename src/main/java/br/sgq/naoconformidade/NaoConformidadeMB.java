package br.sgq.naoconformidade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("naoConformidadeMB")
@ViewScoped
public class NaoConformidadeMB implements Serializable {

	private static final long serialVersionUID = -820733869673362096L;

	@Inject
	private NaoConformidadeService service;
	private List<NaoConformidade> naoConformidades = new ArrayList<>();
	private List<NaoConformidade> naoConformidadesSelecionadas = new ArrayList<>();
	private NaoConformidade naoConformidadeSelecionada = new NaoConformidade();
	private NaoConformidade naoConformidade = new NaoConformidade();

	public NaoConformidadeMB() {

	}

	@PostConstruct
	public void init() {
		this.naoConformidades = service.recuperaNaoConformidades();
		String idNaoConformidade = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("idNaoConformidade");
		if (idNaoConformidade != null) {
			this.naoConformidade = service.recuperaPorId(Integer.valueOf(idNaoConformidade));
		}
	}

	public void cadastrarNaoConformidade() {
		FacesContext faces = FacesContext.getCurrentInstance();
		service.salvar(naoConformidade);
		faces.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, 
				"Cadastro",	"Cadastro conclu�do com sucesso."));
	}

	public void excluiNaoConformidade() {
		service.excluir(naoConformidadeSelecionada);
	}

	public void editarNaoConformidade() {
		NavigationHandler nh = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
		nh.handleNavigation(FacesContext.getCurrentInstance(), null,
				"/privado/cadastronaoconformidade.xhtml?faces-redirect=true&idNaoConformidade="
						+ naoConformidadeSelecionada.getIdNaoConformidade());
	}

	public List<NaoConformidade> getNaoConformidades() {
		return naoConformidades;
	}

	public void setNaoConformidades(List<NaoConformidade> naoConformidades) {
		this.naoConformidades = naoConformidades;
	}

	public List<NaoConformidade> getNaoConformidadesSelecionadas() {
		return naoConformidadesSelecionadas;
	}

	public void setNaoConformidadesSelecionadas(List<NaoConformidade> naoConformidadesSelecionadas) {
		this.naoConformidadesSelecionadas = naoConformidadesSelecionadas;
	}

	public NaoConformidade getNaoConformidadeSelecionada() {
		return naoConformidadeSelecionada;
	}

	public void setNaoConformidadeSelecionada(NaoConformidade naoConformidadeSelecionada) {
		this.naoConformidadeSelecionada = naoConformidadeSelecionada;
	}

	public NaoConformidade getNaoConformidade() {
		return naoConformidade;
	}

	public void setNaoConformidade(NaoConformidade naoConformidade) {
		this.naoConformidade = naoConformidade;
	}

}
