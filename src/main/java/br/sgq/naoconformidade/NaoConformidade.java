package br.sgq.naoconformidade;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="NAO_CONFORMIDADE")
public class NaoConformidade {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer idNaoConformidade;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="prioridade")
	private PrioridadeNaoConformidade prioridade;
	
	@Column(name="data_abertura")
	@Temporal(TemporalType.DATE)
	private Date data;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="responsaveis")
	private String responsaveis;
	
	public NaoConformidade() {
		
	}
	
	public Integer getIdNaoConformidade() {
		return idNaoConformidade;
	}
	public void setIdNaoConformidade(Integer idNaoConformidade) {
		this.idNaoConformidade = idNaoConformidade;
	}
	public PrioridadeNaoConformidade getPrioridade() {
		return prioridade;
	}
	public void setPrioridade(PrioridadeNaoConformidade prioridade) {
		this.prioridade = prioridade;
	}
	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getResponsaveis() {
		return responsaveis;
	}

	public void setResponsaveis(String responsaveis) {
		this.responsaveis = responsaveis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idNaoConformidade == null) ? 0 : idNaoConformidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NaoConformidade other = (NaoConformidade) obj;
		if (idNaoConformidade == null) {
			if (other.idNaoConformidade != null)
				return false;
		} else if (!idNaoConformidade.equals(other.idNaoConformidade))
			return false;
		return true;
	}
	
	
}
