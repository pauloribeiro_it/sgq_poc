package br.sgq.naoconformidade;

import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Named
public class NaoConformidadeDAO {
	
	@PersistenceContext(unitName = "sgq",type=PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;
	
	public void salvar(NaoConformidade naoConformidade) {
		if(naoConformidade.getIdNaoConformidade() != null) {
			naoConformidade = entityManager.merge(naoConformidade);
		}
		entityManager.persist(naoConformidade);
	}
	
	public void excluir(NaoConformidade naoConformidade) {
		NaoConformidade naoConformidadeRemovida = buscaPorId(naoConformidade.getIdNaoConformidade());
		entityManager.remove(naoConformidadeRemovida);
	}
	
	public NaoConformidade buscaPorId(Integer id) {
		return entityManager.find(NaoConformidade.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<NaoConformidade> getAll(){
		final String selectAll = "select n from NaoConformidade n";
		return entityManager.createQuery(selectAll).getResultList();
	}
	
}
