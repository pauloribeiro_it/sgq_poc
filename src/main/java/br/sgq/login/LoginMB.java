package br.sgq.login;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.subject.Subject;

@Named(value="loginMB")
@SessionScoped
public class LoginMB implements Serializable{
	private static final long serialVersionUID = 2474302205221662703L;
	
	@Inject
	private LoginService loginService;
	
	private String username;
	private String password;
	private Usuario usuario;
	
	public LoginMB() {
		
	}
	
	public void login() {
		Subject currentUser = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(username, new Sha256Hash(password).toHex());
		currentUser.getSession(true);
		currentUser.login(token);
		if (null != SecurityUtils.getSubject().getPrincipal()) {
			this.usuario = loginService.recuperaUsuarioPorLogin(username);
			NavigationHandler nh = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
			nh.handleNavigation(FacesContext.getCurrentInstance(), null, "/privado/consultanormas.xhtml?faces-redirect=true");
		}
	}

	public void logout() {
		SecurityUtils.getSubject().logout();
		NavigationHandler nh = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
		nh.handleNavigation(FacesContext.getCurrentInstance(), null, "/login.xhtml?faces-redirect=true");
		((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession().invalidate();
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
