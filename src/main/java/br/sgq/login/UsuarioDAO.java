package br.sgq.login;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Named
public class UsuarioDAO implements Serializable{
	
	private static final long serialVersionUID = 7543306237026188976L;
	@PersistenceContext(unitName = "sgq",type=PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;
	
	public Usuario getUsuarioByLogin(String login) {
		return (Usuario) entityManager.createQuery("select u from Usuario u where u.email=:login").setParameter("login", login).getResultList().get(0);
	}
}
