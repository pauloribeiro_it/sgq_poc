package br.sgq.login;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class LoginService implements Serializable{
	
	private static final long serialVersionUID = 8901698319295818124L;
	@Inject
	private UsuarioDAO usuarioDao;
	
	public Usuario recuperaUsuarioPorLogin(String login) {
		return usuarioDao.getUsuarioByLogin(login);
	}
}
