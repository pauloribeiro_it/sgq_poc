package br.sgq.consumidor;

public interface Endpoints {
	public static final String CONSULTAR_NORMAS_NACIONAIS = "/" + ResourceType.NORMA.getTipo() + "/consultar/nacionais";
	public static final String CONSULTAR_NORMAS_INTERNACIONAIS = "/" + ResourceType.NORMA.getTipo() + "/consultar/internacionais";
	public static final String CONSULTAR_ASSESSORIAS = "/" + ResourceType.ESPECIALIDADE.getTipo() + "/assessoria/consultar";
	public static final String CONSULTAR_CONSULTORIAS = "/" + ResourceType.ESPECIALIDADE.getTipo() + "/consultoria/consultar";
}
