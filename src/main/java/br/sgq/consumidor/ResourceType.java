package br.sgq.consumidor;

public enum ResourceType {
	NORMA("norma"),ESPECIALIDADE("especialidade");
	
	private String tipo;
	
	ResourceType(String tipo) {
		this.tipo = tipo;
	}
	
	
	public String getTipo() {
		return tipo;
	}
}
