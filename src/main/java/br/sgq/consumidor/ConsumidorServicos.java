package br.sgq.consumidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

@Named
public class ConsumidorServicos implements Serializable{

	private static final long serialVersionUID = -7517922147549687392L;
	private static final String URL_BASE = "http://localhost:8080/api/rest";
	private static final int HTTP_OK = 200;
	private ObjectMapper objectMapper = new ObjectMapper();

	public <T> List<T> consultar(Class<T> resourceType,String endpoint) throws IOException {
		URL url = new URL(URL_BASE + endpoint);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String content = "";

		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != HTTP_OK) {
			throw new ErroConsumoServicoException("Erro ao consumir o webservice: " + endpoint);
		}

		try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
			while (br.ready()) {
				content += br.readLine();
			}
		}
		conn.disconnect();
		
		TypeFactory typeFactory = TypeFactory.defaultInstance();
		List<T> objs = objectMapper.readValue(content, typeFactory.constructCollectionType(ArrayList.class, resourceType));
		return objs;
	}

	public String submeter(String endpoint, String parameter) throws Exception {
		URL url = new URL(URL_BASE + endpoint + "/" + parameter);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String content = "";
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Accept", "application/json");
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
			while (br.ready()) {
				content += br.readLine();
			}
		}
		
		return content;
	}
	
}
