package br.sgq.consumidor;

public class ErroConsumoServicoException extends RuntimeException{
	private static final long serialVersionUID = 1536352481699091172L;

	public ErroConsumoServicoException(Throwable exception,String mensagem) {
		super(mensagem, exception);
	}
	
	public ErroConsumoServicoException(String mensagem) {
		super(mensagem);
	}
}
