create database sgq;
use sgq;

DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(150) DEFAULT NULL,
  email varchar(150) DEFAULT NULL,
  PRIMARY KEY (id)
)

INSERT INTO usuario VALUES (1,'usuario','usuario@usuario.com');


DROP TABLE IF EXISTS login;
CREATE TABLE login (
  login varchar(100) NOT NULL DEFAULT '',
  senha varchar(100) DEFAULT NULL,
  id_usuario int(11) DEFAULT NULL,
  ativo tinyint(1) DEFAULT NULL,
  PRIMARY KEY (login),
  KEY fk_login_usuario (id_usuario),
  CONSTRAINT fk_login_usuario FOREIGN KEY (id_usuario) REFERENCES usuario (id)
)

INSERT INTO login VALUES ('usuario@usuario.com','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',1,1);

CREATE TABLE autorizacao (
  login varchar(100) NOT NULL DEFAULT '',
  papel varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (login,papel),
  CONSTRAINT fk_autorizacao FOREIGN KEY (login) REFERENCES login (login)
) 

INSERT INTO autorizacao VALUES ('usuario@usuario.com','user');

DROP TABLE IF EXISTS ocorrencia;

CREATE TABLE ocorrencia (
  id int(11) NOT NULL AUTO_INCREMENT,
  tipo_ocorrencia int(11) NOT NULL,
  data_ocorrencia date NOT NULL,
  descricao varchar(200) NOT NULL,
  PRIMARY KEY (id)
)

INSERT INTO ocorrencia VALUES (8,0,'2019-10-16','Teste'),(9,1,'2019-10-17','Teste');

DROP TABLE IF EXISTS nao_conformidade;

CREATE TABLE nao_conformidade (
  id int(11) NOT NULL AUTO_INCREMENT,
  prioridade int(11) NOT NULL,
  data_abertura date NOT NULL,
  descricao varchar(200) NOT NULL,
  responsaveis varchar(200) DEFAULT NULL,
  PRIMARY KEY (id)
)

INSERT INTO nao_conformidade VALUES (7,0,'2019-10-24','Teste','Jo�o');